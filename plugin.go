package searchfly

import (
	"regexp"
)

type Plugin struct {
	Label      string `json:"label"`
	Matches    []string
	HandleFunc func(c Context, in PluginIn) (PluginOut, error) `json:"-"`

	rg []*regexp.Regexp
}

type PluginIn struct {
	Raw string

	Line1    string
	Line2End string
}

type PluginOut struct {
	Raw string `json:"raw"`

	Links []LinkedItem `json:"links,omitempty"`
	// Markdown string
	// Html     string
	Extra map[string]any `json:"extra"`
}

type LinkedItem struct {
	Label string `json:"label"`
	Link  string `json:"link"`
}
