package searchfly_test

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/xiaofangjian/searchfly"
)

func ExampleApp() {
	app := searchfly.Default()
	app.Use(searchfly.Plugin{
		Label:   "ping-pong",
		Matches: []string{`^\w+`},
		HandleFunc: func(c searchfly.Context, in searchfly.PluginIn) (searchfly.PluginOut, error) {
			return searchfly.PluginOut{Links: []searchfly.LinkedItem{{in.Raw, "pong"}}}, nil
		},
	})

	c := searchfly.Background()

	req := &searchfly.Request{Body: "ping"}
	rsp := app.Handle(c, req)
	fmt.Println(rsp.Results[0].Links[0].Label, rsp.Results[0].Links[0].Link)
	// Output: ping pong
}

func ExampleApp_Use() {
	app := searchfly.Default()
	app.Use(searchfly.Plugin{
		Label:   "sum",
		Matches: []string{`^\d+`},
		HandleFunc: func(c searchfly.Context, in searchfly.PluginIn) (searchfly.PluginOut, error) {
			sum := 0
			for _, s := range strings.Split(in.Raw, ",") {
				i, _ := strconv.Atoi(s)
				sum += i
			}

			return searchfly.PluginOut{Links: []searchfly.LinkedItem{{strconv.Itoa(sum), ""}}}, nil
		},
	})

	c := searchfly.Background()

	req := &searchfly.Request{Body: "1,2"}
	rsp := app.Handle(c, req)
	fmt.Println(rsp.Results[0].Links[0].Label)
	// Output: 3
}
