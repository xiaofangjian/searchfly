package searchfly

import (
	"context"

	"github.com/sirupsen/logrus"
)

type Context interface {
	context.Context

	Log
}

type Log interface {
	logrus.Ext1FieldLogger
}

func Background() Context {
	return FromContext(context.Background())
}

func TODO() Context {
	return FromContext(context.TODO())
}

type ContextImpl struct {
	context.Context

	*logrus.Entry
}

func FromContext(c context.Context) Context {
	return &ContextImpl{
		Context: c,
		Entry:   logrus.WithContext(c),
	}
}
