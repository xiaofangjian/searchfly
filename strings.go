package searchfly

import "strings"

// ShiftLine1 split texts to line1 and line[2..]
func ShiftLine1(s string) (line1 string, body string) {
	i := strings.Index(s, "\n")
	if i < 0 {
		return s, ""
	}

	return s[:i], s[i+1:]
}
