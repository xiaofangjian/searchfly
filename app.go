package searchfly

import (
	"regexp"
	"sync"

	"github.com/samber/lo/parallel"
)

type App struct {
	loadedPlugins []*Plugin

	rw sync.RWMutex
}

func Default() *App {
	app := &App{}
	app.loadedPlugins = []*Plugin{}
	return app
}

func (app *App) Use(plg Plugin) {
	if plg.HandleFunc == nil {
		panic("HandlerFunc is nil")
	}

	if len(plg.Matches) == 0 {
		panic("Matches is empty")
	}

	for _, p := range plg.Matches {
		plg.rg = append(plg.rg, regexp.MustCompile(p))
	}

	app.rw.Lock()
	app.loadedPlugins = append(app.loadedPlugins, &plg)
	app.rw.Unlock()
}

type Request struct {
	Body string
}

type Response struct {
	Results []PluginResult
}

type PluginResult struct {
	Plugin string
	PluginOut
}

func (app *App) Handle(c Context, req *Request) (resp Response) {
	if len(req.Body) == 0 {
		return
	}

	line1, line2end := ShiftLine1(req.Body)

	plugins := app.loadedPlugins
	parallel.ForEach(plugins, func(p *Plugin, i int) {
		matched := false
		for _, rg := range p.rg {
			if rg.MatchString(line1) {
				matched = true
				break
			}
		}

		if !matched {
			return
		}

		pout, err := p.HandleFunc(c, PluginIn{Raw: req.Body, Line1: line1, Line2End: line2end})
		if err != nil {
			c.WithField("plugin", p.Label).WithField("input", req.Body).Warn()
			return
		}

		resp.Results = append(resp.Results, PluginResult{
			PluginOut: pout,
		})
	})

	return
}
