package searchfly

import (
	"testing"
)

func TestShiftLine1(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name      string
		args      args
		wantLine1 string
		wantBody  string
	}{
		// TODO: Add test cases.
		{"", args{"abc"}, "abc", ""},
		{"", args{"abc\n"}, "abc", ""},
		{"", args{"abc\ncc"}, "abc", "cc"},
		{"", args{"abc\ncc\ndd"}, "abc", "cc\ndd"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLine1, gotBody := ShiftLine1(tt.args.s)
			if gotLine1 != tt.wantLine1 {
				t.Errorf("Split12End() gotLine1 = %v, want %v", gotLine1, tt.wantLine1)
			}
			if gotBody != tt.wantBody {
				t.Errorf("Split12End() gotBody = %v, want %v", gotBody, tt.wantBody)
			}
		})
	}
}
